<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::any('data/sensor/push', 'SensorDataController@store');
Route::any('data/sensor/pull', 'SensorDataController@get');


Route::post('/user/login', 'DeviceDataController@login');

Route::post('/data/device', 'DeviceDataController@store');
Route::get('/data/device', 'DeviceDataController@get');

Route::post('/data/sensor', 'SensorDataController@store');
Route::get('/data/sensor', 'SensorDataController@get');

Route::post('/data/app', 'AppDataController@store');
Route::get('/data/app', 'AppDataController@get');