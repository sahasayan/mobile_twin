<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSensorDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensor_data', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->double('accelerometer_x')->nullable();
            $table->double('accelerometer_y')->nullable();
            $table->double('accelerometer_z')->nullable();
            $table->double('gyroscope_x')->nullable();
            $table->double('gyroscope_y')->nullable();
            $table->double('gyroscope_z')->nullable();
            $table->double('compass_x')->nullable();
            $table->double('compass_y')->nullable();
            $table->double('compass_z')->nullable();
            $table->double('proximity')->nullable();
            $table->double('free_ram')->nullable();
            $table->double('light')->nullable();
            $table->integer('timestamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensor_models');
    }
}
