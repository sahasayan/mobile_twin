<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SensorModel extends Model
{
    protected $table = 'sensor_data';
}
