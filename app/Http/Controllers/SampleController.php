<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\StreamedResponse;

class SampleController extends Controller
{
    public function get() {

        $response = new StreamedResponse(function () {
            $i = 0;
            while (true) {
                $i++;
                echo "id: {$i}\n";
                echo 'data: ' . date('m/d/Y h:i:s a', time()) . "\n\n";
                ob_flush();
                flush();

                $res_per_second = 6;
                usleep((1000 / $res_per_second) * 1000);
            }
        });

        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');

        return $response;
    }
}
