<?php

namespace App\Http\Controllers;

use App\Model\SensorModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Ramsey\Uuid\Uuid;

class SensorDataController extends Controller
{
    public function store(Request $request)
    {
        Log::debug($request->fullUrl());
        Log::debug($request->all());

        try {
            DB::beginTransaction();
            $sensor = new SensorModel();
            $sensor->uuid = Uuid::uuid4()->toString();
            $sensor->accelerometer_x = $request->get('accx', null);
            $sensor->accelerometer_y = $request->get('accy', null);
            $sensor->accelerometer_z = $request->get('accz', null);
            $sensor->gyroscope_x = $request->get('gyrox', null);
            $sensor->gyroscope_y = $request->get('gyroy', null);
            $sensor->gyroscope_z = $request->get('gyroz', null);
            $sensor->compass_x = $request->get('compassx', null);
            $sensor->compass_y = $request->get('compassy', null);
            $sensor->compass_z = $request->get('compassz', null);
            $sensor->proximity = $request->get('proximity', null);
            $sensor->free_ram = $request->get('free_ram', null);
            $sensor->light = $request->get('flux', null);
            $sensor->timestamp = $request->get('timestamp', -1);
            $sensor->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getMessage());
            return [
                'success' => false
            ];
        }

        return [
            'success' => true
        ];
    }

    public function get(Request $request)
    {
        try {
            $data = SensorModel::orderBy('created_at', 'desc')->limit($request->get('limit', 50))->get();

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return [
                'success' => false
            ];
        }

        return $data;
    }
}
