<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

class DeviceDataController extends Controller
{
    public function store(Request $request)
    {
        Log::debug($request->fullUrl());
        Log::debug($request->all());

        return [
            'success' => true
        ];
    }

    public function get(Request $request)
    {
        return [];
    }

    public function login(Request $request)
    {
        if ($request->get('user_id') == 'test' &&
            $request->get('password') == 'test') {
            return [
                'success' => true,
                'access_token' => Uuid::uuid4()
            ];
        } else {
            return [
                'success' => false
            ];
        }
    }
}
