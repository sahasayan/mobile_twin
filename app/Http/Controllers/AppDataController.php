<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AppDataController extends Controller
{
    public function store(Request $request)
    {
        Log::debug($request->fullUrl());
        Log::debug($request->all());

        return [
            'success' => true
        ];
    }

    public function get(Request $request)
    {
        return [];
    }
}
